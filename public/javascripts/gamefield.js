class GameField {

    #state;
    #mode;
    #isOverGame;

    constructor(n) {
        this.#state = new Array(n);
        for (let i = 0; i < n; i++) {
            this.#state[i] = new Array(n);
            for (let j = 0; j < n; j++) {
                this.#state[i][j] = null;
            }
        }

        // false - крестик
        this.#mode = false;

        this.#isOverGame = false;
    }
    
    getGameFieldStatus() {
        return this.#state;
    }

    getMode() {
        return this.#mode;
    }

    setMode(mode) {
        this.#mode = mode;
    }

    getIsOverGame() {
        return this.#isOverGame;
    }

    FieldCellValue() {
        let row = -1;
        let column = -1;

        while (true) {
            row = prompt("Введите строку (целое число в диапазоне [1; n])");

            while (isNaN(row)) {
                row = prompt("Строка должна быть числом. Попробуй ввести ещё раз");
            }

            while (!Number.isInteger(Number(row))) {
                row = prompt("Строка должны быть целым числом. Попробуй ввести ещё раз");
            }

            row = Number(row);

            while (row < 1) {
                row = prompt("Строка должны быть >= 1. Попробуй ввести ещё раз");
            }

            while (row > this.#state.length) {
                row = prompt("Строка должны быть <= " + this.#state.length + "Попробуй ввести ещё раз");
            }
    
            column = prompt("Введите столбец (целое число в диапазоне [1; n])");

            while (isNaN(column)) {
                column = prompt("Строка должна быть числом. Попробуй ввести ещё раз");
            }

            while (!Number.isInteger(Number(column))) {
                row = prompt("Строка должны быть целым числом. Попробуй ввести ещё раз");
            }

            column = Number(column);

            while (column < 1) {
                row = prompt("Строка должны быть >= 1. Попробуй ввести ещё раз");
            }

            while (column > this.#state.length) {
                row = prompt("Строка должны быть <= " + this.#state.length + "Попробуй ввести ещё раз");
            }

            if (this.#state[row-1][column-1] == null) {
                if (this.#mode) {
                    this.#state[row-1][column-1] = 'o';
                } else {
                    this.#state[row-1][column-1] = 'x';
                }
                return;
            }
            
            alert("Клетка занята. Попробуйте другую");
        }
    }

    fillCellValue(oneDimIndex) {
        if (this.#mode) {
            this.#state[Math.trunc(oneDimIndex / 3)][oneDimIndex % 3] = 'o';
        } else {
            this.#state[Math.trunc(oneDimIndex / 3)][oneDimIndex % 3] = 'x';
        }
    }

    // Метод обхода доски
    gameFieldTraversal() {
        const n = this.#state.length;
    
        let diagSum = 0;
        let antiDiagSum = 0;
    
        // Наличие null в матрице говорит о том, что игра продолжается
        let isGameContinues = false;
    
        for (let i = 0; i < n; i++) {
            let rowSum = 0;
            let colSum = 0;
            for (let j = 0; j < n; j++) {
    
                // Обход строк
                if (this.#state[i][j] === 'x') {
                    rowSum++;
                } else if (this.#state[i][j] === 'o') {
                    rowSum--;
                } else {
                    isGameContinues = true;
                }
    
                // Обход столбцов
                if (this.#state[j][i] === 'x') {
                    colSum++;
                } else if (this.#state[j][i] === 'o') {
                    colSum--;
                } else {
                    isGameContinues = true;
                }
    
                // Обход главной диагонали
                if (i === j) {
                    if (this.#state[i][j] === 'x') {
                        diagSum++;
                    } else if (this.#state[i][j] === 'o') {
                        diagSum--;
                    } else {
                        isGameContinues = true;
                    }
                }
    
                // Обход побочной диагонали
                if (i == n - 1 - j) {
                    if (this.#state[i][j] === 'x') {
                        antiDiagSum++;
                    } else if (this.#state[i][j] === 'o') {
                        antiDiagSum--;
                    } else {
                        isGameContinues = true;
                    }
                }
                if (antiDiagSum === n) {
                    this.#isOverGame = true;
                    return "Крестики выиграли";
                }
                if (antiDiagSum === -n) {
                    this.#isOverGame = true;
                    return "Нолики выиграли";
                }
            }
            
            if (rowSum === n || colSum === n) {
                this.#isOverGame = true;
                return "Крестики выиграли";
            }
            if (rowSum === -n || colSum === -n) {
                this.#isOverGame = true;
                return "Нолики выиграли";
            }
        }
    
        if (diagSum === n) {
            this.#isOverGame = true;
            return "Крестики выиграли";
        }
        if (diagSum === -n) {
            this.#isOverGame = true;
            return "Нолики выиграли";
        }
    
        if (isGameContinues) {
            return "Игра продолжается";
        }
        
        this.#isOverGame = true;
        return "Ничья";
    }
}

const gameField = new GameField(3);

while (!gameField.getIsOverGame()) {
    gameField.FieldCellValue();
    gameField.setMode(!gameField.getMode());
    gameField.gameFieldTraversal();
}

alert(gameField.gameFieldTraversal());
