var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
const bcrypt = require('bcrypt');
const fs = require('fs');
const path = require('path');

/* GET home page. */
router.get('/', function(req, res, next) {
  if (!req.session.user) {
    res.render('pages/auth');
    return;
  }
  res.status(403).send("Вы уже авторизованы. Чтобы ещё раз авторизоваться, перезапустите сервер");
});

var users =  {
  artem: {name: "Artem"}
};

function authenticate(login, password, fn) {
  var user = false;

  for (let user_key in users) {
    if (users[user_key]["name"] === login) {
      user = user_key;
      break;
    }
  }

  if (!user) {
    return fn(null, null);
  }

  fs.readFile(path.join(__dirname, 'password.txt'), 'utf-8', (err, hash) => {
    if (err) {
      console.log(__dirname + err);
      return fn(err);
    }
    bcrypt.compare(password, hash, (err, result) => {
      if (result) {
        return fn(null, user);
      }
      fn(null, null);
    });
  });
}

var urlencodedParser = bodyParser.urlencoded({ extended: false });
router.post('/login', urlencodedParser, (req, res, next) => {
  authenticate(req.body.login, req.body.password, (err, user) => {
    if (err) {
      return next(err);
    }

    if (user) {
      req.session.regenerate(() => {
        req.session.user = user;
        req.session.success = 'Authenticated as ' + user.name
        + ' You may now access <a href="/rating">/rating</a>.';
      res.redirect('/rating');
    });
    } else {
      req.session.error = 'Authentication failed, please check your '
      + ' username and password.'
      + ' (use "Artem" and "1234")';
      res.render('pages/auth', {invalidAuth: true});
    }
  });
});

module.exports = router;
