var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  if (req.session.user) {
    var users = [
      {name: "Александров Игнат Анатолиевич", totalGames: 24534, wins: 9824, loses: 1222, winningPercentage: 87},
      {name: "Низамов Артем Рустамович", totalGames: 24535, wins: 1222, loses: 9824, winningPercentage: 13}
    ];
  
    res.render('pages/rating', {users: users});
    return;
  }
  res.status(403).send("Я запрещаю вам заходить в рейтинг :(");
});

module.exports = router;